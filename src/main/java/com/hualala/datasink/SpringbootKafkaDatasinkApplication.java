package com.hualala.datasink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootKafkaDatasinkApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootKafkaDatasinkApplication.class, args);
    }

}
